package chat;


import message.Message;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.net.SocketException;
import java.util.Vector;

public class ClientConnection extends Thread {

    private Socket socket;
    private Vector<String> messagesVector;
    private int backMessagesNum = 3;

    private Server.MessageListener regListener;
    private Server.MessageListener textListener;

    private ObjectInputStream inp;
    private ObjectOutputStream out;

    public ClientConnection(Socket clientSocket, Vector<String> messagesVector) {
        this.socket = clientSocket;
        this.messagesVector = messagesVector;
    }

    @Override
    public void run() {
        super.run();
        try {
            inp = new ObjectInputStream(socket.getInputStream());
            out = new ObjectOutputStream(socket.getOutputStream());
        } catch (IOException e) {
            System.err.println("Cannot get stream from connection");
            return;
        }

        Message rcvMessage = null;
        while (true) {
            try {
                rcvMessage = (Message) inp.readObject();
                if (rcvMessage == null) {
                    socket.close();
                    return;
                } else {
                    if (rcvMessage.getType() == Message.Type.REGISTRATION) {
                        int last = messagesVector.size();
                        for (int i = 0; i < backMessagesNum; ++i) {
                            if (last - backMessagesNum + i < 0) continue;
                            Message msg = new Message();
                            msg.setSender(messagesVector.get(last - backMessagesNum + i).split(":")[0]);
                            msg.setText(messagesVector.get(last - backMessagesNum + i));
                            send(msg);
                        }
                        regListener.actionPerformed(rcvMessage);
                    } else if (rcvMessage.getType() == Message.Type.MESSAGE){
                        Message msg = new Message();
                        msg.setSender(rcvMessage.getSender());
                        msg.setText(rcvMessage.getSender() + ": " + rcvMessage.getText());
                        messagesVector.add(rcvMessage.getSender() + ": " + rcvMessage.getText());
                        System.out.println("add msg " + rcvMessage.getSender() + ": " + rcvMessage.getText());
                        textListener.actionPerformed(msg);
                    } else {
                        Message msg = new Message();
                        msg.setSender(rcvMessage.getSender());
                        msg.setText(rcvMessage.getSender() + ": " + rcvMessage.getText());
                        textListener.actionPerformed(msg);
                    }
                }
            } catch (SocketException e) {
                Message msg = new Message();
                msg.setText(rcvMessage.getSender() + " left the chat");
                messagesVector.add(rcvMessage.getText());
                textListener.actionPerformed(msg);
                return;
            } catch (IOException e) {
                e.printStackTrace();
                return;
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }
        }
    }

    public void setOnRegListener(Server.MessageListener regListener) {
        this.regListener = regListener;
    }

    public void setOnTextListener(Server.MessageListener textListener) {
        this.textListener = textListener;
    }

    public void send(Message msg) {
        try {
            out.flush();
            out.writeObject(msg);
        } catch (IOException e) {
            return;
        }
    }
}
