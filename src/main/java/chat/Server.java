package chat;

import message.Message;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Vector;

public class Server implements Runnable{

    private ServerSocket serverSocket;
    private ArrayList<ClientConnection> connections;
    private Vector<String> messages;


    public Server(int port) throws IOException {
        serverSocket = new ServerSocket(port);
        connections = new ArrayList<>(10);
        messages = new Vector<>(10);
    }

    public void run() {
        while (true) {
            try {
                Socket socket = serverSocket.accept();
                ClientConnection clientConnection = new ClientConnection(socket, messages);
                clientConnection.setOnRegListener((msg -> {
                    msg.setText(msg.getSender() + " has just joined the chat");
                    broadcast(msg);
                }));
                clientConnection.setOnTextListener(this::broadcast);
                connections.add(clientConnection);
                clientConnection.start();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public void broadcast(Message msg) {
        for (ClientConnection connection: connections) {
            connection.send(msg);
        }
    }

    public interface MessageListener {
        void actionPerformed(Message msg);
    }
}
