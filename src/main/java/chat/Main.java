package chat;

import java.io.IOException;

public class Main {
    public static void main(String[] args) {
        try {
            new Thread(new Server(Integer.valueOf(args[0]))).start();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
